/***********************************************************************************************/
// Fakulta informatiky, Vysok� u�en� technick� v Brn�
// Diplomov� pr�ce - Elektronov� raytracer
// DIP2019
// Autor: Jan Such�nek
// Vedouc�: Doc. Ing. Martin �ad�k, Ph.D.
//
///////////////////////////////////////////////////////////////////////////////////////////////
//
// Soubor: electronGun.h
// Deklarace vlastn�ho jednoduch�ho zdroje elektron�
/***********************************************************************************************/

#if defined(_MSC_VER)
#define NOMINMAX
#pragma once
#endif

#ifndef PBRT_CAMERAS_ELECTRON_GUN_H
#define PBRT_CAMERAS_ELECTRON_GUN_H

// cameras/orthographic.h*
#include "pbrt.h"
#include "camera.h"
#include "orthographic.h"
#include "film.h"

#include "rng.h"

#include <random>

namespace pbrt {

    class FloatRangeGenerator {
  public:
        
    FloatRangeGenerator() {}
    FloatRangeGenerator(float min, float max)
        : e2(std::random_device()()), dist(min, max) { 
    }
    float next(std::mt19937 engine) const { 
       return 100.f; // unable to use random variable
    }
    std::mt19937 e2;
    private:
          std::uniform_real_distribution<float> dist;
    };

  // OrthographicCamera Declarations
class ElectronGun : public OrthographicCamera {
  public:
    // OrthographicCamera Public Methods
    ElectronGun(float energyMin, float energyMax, const AnimatedTransform &CameraToWorld,
                       const Bounds2f &screenWindow, Float shutterOpen,
                       Float shutterClose, Float lensRadius,
                       Float focalDistance, Film *film, const Medium *medium) : rndEnergy(energyMin, energyMax),
                       OrthographicCamera(CameraToWorld, screenWindow,
                           shutterOpen, shutterClose, lensRadius, focalDistance,
                           film, medium) {
        // Compute differential changes in origin for orthographic camera rays
        dxCamera = RasterToCamera(Vector3f(1, 0, 0));
        dyCamera = RasterToCamera(Vector3f(0, 1, 0));
        
        this->energyMax = energyMax;
        this->energyMin = energyMin;
    }
    Float GenerateRay(const CameraSample &sample, Ray *) const;
    Float GenerateElectronRay(const CameraSample &sample,
                                  ElectronRay *) const;
  private:
    // OrthographicCamera Private Data
    Vector3f dxCamera, dyCamera;
    float energyMin;
    float energyMax;
    FloatRangeGenerator rndEnergy;
};


ElectronGun *CreateElectronGun(const ParamSet &params,
                                             const AnimatedTransform &cam2world,
                                             Film *film, const Medium *medium);



}  // namespace pbrt

#endif  // PBRT_CAMERAS_ELECTRON_GUN_H
