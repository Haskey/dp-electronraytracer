/***********************************************************************************************/
// Fakulta informatiky, Vysok� u�en� technick� v Brn�
// Diplomov� pr�ce - Elektronov� raytracer
// DIP2019
// Autor: Jan Such�nek
// Vedouc�: Doc. Ing. Martin �ad�k, Ph.D.
//
///////////////////////////////////////////////////////////////////////////////////////////////
//
// Soubor: electrohDetector.h
// Deklarace vlastn�ho jednoduch�ho elektronov�ho detektoru.
/***********************************************************************************************/


#if defined(_MSC_VER)
#define NOMINMAX
#pragma once
#endif

#ifndef PBRT_LIGHTS_ELECTRON_DETECTOR_H
#define PBRT_LIGHTS_ELECTRON_DETECTOR_H

// lights/point.h*
#include "light.h"
#include "pbrt.h"
#include "shape.h"

namespace pbrt {

// PointLight Declarations
class ElectronDetector : public Light {
  public:
    // PointLight Public Methods
    ElectronDetector(const Transform &LightToWorld,
                     const MediumInterface &mediumInterface, const Spectrum &I,
                     bool (*magneticFieldFnc)(float r, RayDifferential ray))
        : Light((int)LightFlags::DeltaPosition, LightToWorld, mediumInterface),
          pLight(LightToWorld(Point3f(0, 0, 0))),
          I(I),
          magneticFieldFnc(magneticFieldFnc) {}
    Spectrum Sample_Li(const Interaction &ref, const Point2f &u, Vector3f *wi,
                       Float *pdf, VisibilityTester *vis) const;
    Spectrum Power() const;
    Float Pdf_Li(const Interaction &, const Vector3f &) const;
    Spectrum Sample_Le(const Point2f &u1, const Point2f &u2, Float time,
                       Ray *ray, Normal3f *nLight, Float *pdfPos,
                       Float *pdfDir) const;
    void Pdf_Le(const Ray &, const Normal3f &, Float *pdfPos,
                Float *pdfDir) const;

	bool DetectorHit(RayDifferential ray) {
		return magneticFieldFnc(0.5, ray);
	}

    float MagneticFieldForce = 1.f;
	//
	// Magnetic field functions 
    // Zde lze dodefinovat vlastn� funkci elmg pole elektronov�ho detektoru.
	//
    static bool SphericalMagneticField(float radius, RayDifferential ray) {
        return false;
    }

    static bool RandomMagneticField(float radius, RayDifferential ray) {
        return (rand() % 2 == 0);
    } 
    static bool ThresholdMagneticField(float radius, RayDifferential ray) {
        return ray.o.y > 1.0f;
    } 
    
    bool (*magneticFieldFnc)(float radius, RayDifferential ray);

  private:
    // PointLight Private Data
    const Point3f pLight;
    const Spectrum I;
    

   
};

std::shared_ptr<ElectronDetector> CreateElectronDetector(
    const Transform &light2world, const Medium *medium,
    const ParamSet &paramSet);

}  // namespace pbrt

#endif  // PBRT_LIGHTS_ELECTRON_DETECTOR_H
