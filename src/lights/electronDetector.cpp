/***********************************************************************************************/
// Fakulta informatiky, Vysok� u�en� technick� v Brn�
// Diplomov� pr�ce - Elektronov� raytracer
// DIP2019
// Autor: Jan Such�nek
// Vedouc�: Doc. Ing. Martin �ad�k, Ph.D.
//
///////////////////////////////////////////////////////////////////////////////////////////////
//
// Soubor: electronDetector.cpp
// Implementace jednoduch�ho detektoru.
/***********************************************************************************************/

#include "lights/electronDetector.h"
#include "paramset.h"
#include "sampling.h"
#include "scene.h"
#include "stats.h"

namespace pbrt {

// PointLight Method Definitions
Spectrum ElectronDetector::Sample_Li(const Interaction &ref, const Point2f &u,
                               Vector3f *wi, Float *pdf,
                               VisibilityTester *vis) const {
    ProfilePhase _(Prof::LightSample);
    *wi = Normalize(pLight - ref.p);
    *pdf = 1.f;
    *vis =
        VisibilityTester(ref, Interaction(pLight, ref.time, mediumInterface));
    return I / DistanceSquared(pLight, ref.p);
}

Spectrum ElectronDetector::Power() const { return 4 * Pi * I; }

Float ElectronDetector::Pdf_Li(const Interaction &, const Vector3f &) const {
    return 0;
}

Spectrum ElectronDetector::Sample_Le(const Point2f &u1, const Point2f &u2,
                                     Float time,
                               Ray *ray, Normal3f *nLight, Float *pdfPos,
                               Float *pdfDir) const {
    ProfilePhase _(Prof::LightSample);
    *ray = Ray(pLight, UniformSampleSphere(u1), Infinity, time,
               mediumInterface.inside);
    *nLight = (Normal3f)ray->d;
    *pdfPos = 1;
    *pdfDir = UniformSpherePdf();
    return I;
}

void ElectronDetector::Pdf_Le(const Ray &, const Normal3f &, Float *pdfPos,
                        Float *pdfDir) const {
    ProfilePhase _(Prof::LightPdf);
    *pdfPos = 0;
    *pdfDir = UniformSpherePdf();
}

std::shared_ptr<ElectronDetector> CreateElectronDetector(
    const Transform &light2world, const Medium *medium,
    const ParamSet &paramSet) {
    Spectrum I = paramSet.FindOneSpectrum("I", Spectrum(1.0));
    Spectrum sc = paramSet.FindOneSpectrum("scale", Spectrum(1.0));
    Point3f P = paramSet.FindOnePoint3f("from", Point3f(0, 0, 0));
    Transform l2w = Translate(Vector3f(P.x, P.y, P.z)) * light2world;

    return std::make_shared<ElectronDetector>(
        l2w, medium, I * sc, &ElectronDetector::ThresholdMagneticField);
}

}  // namespace pbrt
