/***********************************************************************************************/
// Fakulta informatiky, Vysok� u�en� technick� v Brn�
// Diplomov� pr�ce - Elektronov� raytracer
// DIP2019
// Autor: Jan Such�nek
// Vedouc�: Doc. Ing. Martin �ad�k, Ph.D.
//
///////////////////////////////////////////////////////////////////////////////////////////////
//
// Soubor: electron.h
// Integr�tor pro elektronov� detektor.
//
/***********************************************************************************************/

#if defined(_MSC_VER)
#define NOMINMAX
#pragma once
#endif

#ifndef PBRT_INTEGRATORS_ELECTRON_H
#define PBRT_INTEGRATORS_ELECTRON_H

// integrators/path.h*
#include "integrator.h"
#include "interaction.h"
#include "lightdistrib.h"
#include "lights/electronDetector.h"
#include "pbrt.h"

#include <random>

namespace pbrt {

bool StraightInterpolation(ElectronRay SEray, ElectronDetector *detector,
                           Scene scene, SurfaceInteraction PERayInteraction,
                           Point2f point);
bool NStepInterpolation(ElectronRay SEray, ElectronDetector *detector,
                        Scene scene, SurfaceInteraction PERayInteraction,
                        Point2f point);
// PathIntegrator Declarations
class ElectronIntegrator : public ElectronIntegratorBase {
  public:
    // PathIntegrator Public Methods
    ElectronIntegrator(int maxDepth, std::shared_ptr<const Camera> camera,
                       std::shared_ptr<Sampler> sampler,
                       const Bounds2i &pixelBounds, Float rrThreshold = 1,
                       const std::string &lightSampleStrategy = "spatial",
                       bool (*Interpolate)(ElectronRay, ElectronDetector *,
                                           Scene, SurfaceInteraction, Point2f) = &StraightInterpolation);

    void Preprocess(const Scene &scene, Sampler &sampler);
    Spectrum Li(const RayDifferential &ray, const Scene &scene,
                Sampler &sampler, MemoryArena &arena, int depth) const;
    Spectrum Li(const ElectronRay &ray, const Scene &scene, Sampler &sampler,
                MemoryArena &arena, int depth) const;

  private:
    // PathIntegrator Private Data
    const int maxDepth;
    const Float rrThreshold;
    const std::string lightSampleStrategy;
    std::unique_ptr<LightDistribution> lightDistribution;

    bool (*Interpolate)(ElectronRay, ElectronDetector *, Scene, SurfaceInteraction,
                        Point2f);

    int samplesPerPixel = 10;
};


ElectronIntegrator *CreateElectronIntegrator(
    const ParamSet &params, std::shared_ptr<Sampler> sampler,
    std::shared_ptr<const Camera> camera);

}  // namespace pbrt

#endif  // PBRT_INTEGRATORS_ELECTRON_H
