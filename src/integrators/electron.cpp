/***********************************************************************************************/
// Fakulta informatiky, Vysok� u�en� technick� v Brn�
// Diplomov� pr�ce - Elektronov� raytracer
// DIP2019
// Autor: Jan Such�nek
// Vedouc�: Doc. Ing. Martin �ad�k, Ph.D.
//
///////////////////////////////////////////////////////////////////////////////////////////////
//
// Soubor: electron.cpp
// V�po�et trasy elektronov�ch paprsk�.
/***********************************************************************************************/

// integrators/path.cpp*
#include "integrators/electron.h"
#include "bssrdf.h"
#include "camera.h"
#include "film.h"

#include "paramset.h"
#include "scene.h"
#include "stats.h"

#include "materials/microscopeMaterial.h"

namespace pbrt {

STAT_PERCENT("Integrator/Zero-radiance paths", zeroRadiancePaths, totalPaths);
STAT_INT_DISTRIBUTION("Integrator/Path length", pathLength);

// Electron Method Definitions
ElectronIntegrator::ElectronIntegrator(int maxDepth,
                                       std::shared_ptr<const Camera> camera,
                                       std::shared_ptr<Sampler> sampler,
                                       const Bounds2i &pixelBounds,
                                       Float rrThreshold,
                                       const std::string &lightSampleStrategy,
                                       bool (*Interpolate)(ElectronRay, ElectronDetector *, Scene, SurfaceInteraction, Point2f) )
    : ElectronIntegratorBase(camera, sampler, pixelBounds),
      maxDepth(maxDepth),
      rrThreshold(rrThreshold),
      lightSampleStrategy(lightSampleStrategy) {
    this->Interpolate = Interpolate;
}

void ElectronIntegrator::Preprocess(const Scene &scene, Sampler &sampler) {
    lightDistribution =
        CreateLightSampleDistribution(lightSampleStrategy, scene);
}

Spectrum ElectronIntegrator::Li(const RayDifferential &r, const Scene &scene,
                                Sampler &sampler, MemoryArena &arena,
                                int depth) const 
{
    ProfilePhase p(Prof::SamplerIntegratorLi);
    Spectrum L(0.f), beta(0.0f);
    RayDifferential ray(r);

    SurfaceInteraction isect;
    bool foundIntersection = scene.Intersect(ray, &isect);
    auto light = scene.lights.at(1);
    ElectronDetector *detector = dynamic_cast<ElectronDetector *>(light.get());

    if (foundIntersection) {
        float cosphi = AbsDot(ray.d, isect.n) /*/ ray.d.Length() * isect.shading.n.Length()*/;
        auto phi = acos(cosphi);
        auto material = dynamic_cast<const MicroscopeMaterial*>( isect.primitive->GetMaterial());
        if (material == nullptr) {
            // handle error    
        }
        
        auto SEyield = material->GetSEYield(phi);


        int SErayNum = round(SEyield);
        for (int i = 0; i < SErayNum; i++) {
            isect.ComputeScatteringFunctions(ray, arena);
            if (!isect.bsdf) {
                continue;
            }

            // Sample BSDF to get new path direction 
            Vector3f wo = -ray.d, wi;
            Float pdf;
            BxDFType flags;
            Spectrum f = isect.bsdf->Sample_f(wo, &wi, sampler.Get2D(), &pdf,
                                              BSDF_ALL, &flags);

            Ray SERay = isect.SpawnRay(wi);
            // interpolate SE electron path between detector and PE beam intersection with surface.
            // 
            bool detectorHit = Interpolate(SERay, detector, scene, isect, sampler.Get2D());
            
            detectorHit ? beta += 0.3f : beta = beta;
        }
    }
    L = beta;
    return L;
}

Spectrum ElectronIntegrator::Li(const ElectronRay &r, const Scene &scene,
                                Sampler &sampler, MemoryArena &arena,
                                int depth) const {
    ProfilePhase p(Prof::SamplerIntegratorLi);
    Spectrum L(0.f), beta(0.0f);
    ElectronRay ray(r);

    SurfaceInteraction isect;
    bool foundIntersection = scene.Intersect(ray, &isect);
    auto light = scene.lights.at(1);
    ElectronDetector *detector = dynamic_cast<ElectronDetector *>(light.get());

    if (foundIntersection) {
        float cosphi = AbsDot(
            ray.d, isect.n) /*/ ray.d.Length() * isect.shading.n.Length()*/;
        auto phi = acos(cosphi);
        auto material = dynamic_cast<const MicroscopeMaterial *>(
            isect.primitive->GetMaterial());
        if (material == nullptr) {
            // handle error
        }

        auto SEyield = material->GetSEYield(phi);

        int SErayNum = round(SEyield);
        for (int i = 0; i < SErayNum; i++) {
            isect.ComputeScatteringFunctions(ray, arena);
            if (!isect.bsdf) {
                continue;
            }

            // Sample BSDF to get new path direction
            Vector3f wo = -ray.d, wi;
            Float pdf;
            BxDFType flags;
            Spectrum f = isect.bsdf->Sample_f(wo, &wi, sampler.Get2D(), &pdf,
                                              BSDF_ALL, &flags);

            ElectronRay SERay(isect.SpawnRay(wi));
            SERay.setEnergy(30.f);
            // interpolate SE electron path between detector and PE beam
            // intersection with surface.
            //
            bool detectorHit =
                Interpolate(SERay, detector, scene, isect, sampler.Get2D());

            detectorHit ? beta += 0.05f : beta = beta;
        }
    }
    L = beta;
    return L;
}

bool StraightInterpolation(ElectronRay SEray, ElectronDetector *detector, Scene scene, SurfaceInteraction PERayInteraction, Point2f point) {
    SurfaceInteraction SEisect;
    bool foundIntersection = scene.Intersect(SEray, &SEisect);
    return !foundIntersection; // if ray doesnt hit surface, return true
}

bool NStepInterpolation(ElectronRay SEray, ElectronDetector *detector, Scene scene, SurfaceInteraction PERayInteraction, Point2f point) {
    unsigned int noOfSteps = 10;
    SurfaceInteraction SEisect(PERayInteraction);
    Vector3f wi;
    Vector3f tmpD = SEray.d;
    float pdf;
    VisibilityTester visTest;
    ElectronRay tmpRay(SEray);
    tmpRay.setEnergy(SEray.energy);
    // Iterpolate ray path in noOfSteps 
    for (unsigned int i = 0; i < noOfSteps; i++) {
        detector->Sample_Li(SEisect,point, &wi, &pdf, &visTest);
        auto energy = (tmpRay.energy - detector->MagneticFieldForce);

        tmpD = Normalize(tmpD + wi);
        SEisect.p += tmpD * energy/100.0f;
        tmpRay = SEisect.SpawnRay(tmpD);
        tmpRay.setEnergy(energy);

        if (scene.Intersect(tmpRay, &SEisect)) {
            if (energy > 30.f) {
                return NStepInterpolation(tmpRay, detector, scene, SEisect, point);
            }
            return false;
        }

        if (energy < 0.0f) {
            return detector->DetectorHit(tmpRay);
        }

    }
    tmpRay = SEisect.SpawnRay(tmpD);
    return !scene.Intersect(tmpRay, &SEisect);
}

ElectronIntegrator *CreateElectronIntegrator(
    const ParamSet &params, std::shared_ptr<Sampler> sampler,
    std::shared_ptr<const Camera> camera) {
    int maxDepth = params.FindOneInt("maxdepth", 5);
    int np;
    const int *pb = params.FindInt("pixelbounds", &np);
    Bounds2i pixelBounds = camera->film->GetSampleBounds();
    if (pb) {
        if (np != 4)
            Error("Expected four values for \"pixelbounds\" parameter. Got %d.",
                  np);
        else {
            pixelBounds = Intersect(pixelBounds,
                                    Bounds2i{{pb[0], pb[2]}, {pb[1], pb[3]}});
            if (pixelBounds.Area() == 0)
                Error("Degenerate \"pixelbounds\" specified.");
        }
    }
    Float rrThreshold = params.FindOneFloat("rrthreshold", 1.);
    std::string lightStrategy =
        params.FindOneString("lightsamplestrategy", "spatial");

    std::string interpolationMethod =
        params.FindOneString("interpolationMethod", "simpleInterpolation");

    if (interpolationMethod == "simpleInterpolation")
        return new ElectronIntegrator(maxDepth, camera, sampler, pixelBounds,
                                      rrThreshold, lightStrategy,
                                      &StraightInterpolation);
    else if (interpolationMethod == "nStepInterpolation")
        return new ElectronIntegrator(maxDepth, camera, sampler, pixelBounds,
                                      rrThreshold, lightStrategy,
                                      &NStepInterpolation);

    return new ElectronIntegrator(maxDepth, camera, sampler, pixelBounds,
                                  rrThreshold, lightStrategy);
}

}  // namespace pbrt
