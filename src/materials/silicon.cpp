/***********************************************************************************************/
// Fakulta informatiky, Vysok� u�en� technick� v Brn�
// Diplomov� pr�ce - Elektronov� raytracer
// DIP2019
// Autor: Jan Such�nek
// Vedouc�: Doc. Ing. Martin �ad�k, Ph.D.
//
///////////////////////////////////////////////////////////////////////////////////////////////
//
// Soubor: Silicon.cpp
// Implementace materi�lu a jeho vlastnost�
/***********************************************************************************************/

#include "interaction.h"
#include "materials/silicon.h"
#include "paramset.h"
#include "reflection.h"
#include "spectrum.h"
#include "texture.h"

namespace pbrt {

// PlasticMaterial Method Definitions
void SiliconMaterial::ComputeScatteringFunctions(
    SurfaceInteraction *si, MemoryArena &arena, TransportMode mode,
    bool allowMultipleLobes) const {
    // Initialize diffuse component of plastic material
    Spectrum kd = Kd->Evaluate(*si).Clamp();
    si->bsdf = ARENA_ALLOC(arena, BSDF)(*si);
    si->bsdf->Add(ARENA_ALLOC(arena, LambertianReflection)(kd));

    // Initialize specular component of plastic material
   // Spectrum ks = Ks->Evaluate(*si).Clamp();
}

float SiliconMaterial::GetSEYield(float phi) const {
    auto res = secans(phi) + 2.5 * GetEthaForAngle(phi);
    res *= delta0;

    return round(res);
}

float SiliconMaterial::GetEthaForAngle(float angle) const {
    auto tmp = etha0 / BFactor;
    auto etha = BFactor * powf(tmp, cos(angle));
    return etha;
}

float SiliconMaterial::GetDeltaForAngle(float angle) const {
    auto tmp = delta0 * secans(angle);
    return tmp;
}


SiliconMaterial *CreateSiliconMaterial(const TextureParams &mp) {
    std::shared_ptr<Texture<Spectrum>> Kd =
        mp.GetSpectrumTexture("Kd", Spectrum(0.25f));
    std::shared_ptr<Texture<Spectrum>> Ks =
        mp.GetSpectrumTexture("Ks", Spectrum(0.25f));
    std::shared_ptr<Texture<Float>> roughness =
        mp.GetFloatTexture("roughness", .1f);
    std::shared_ptr<Texture<Float>> bumpMap =
        mp.GetFloatTextureOrNull("bumpmap");
    bool remapRoughness = mp.FindBool("remaproughness", false);
    return new SiliconMaterial(Kd, Ks, roughness, bumpMap, remapRoughness);
}

}  // namespace pbrt
