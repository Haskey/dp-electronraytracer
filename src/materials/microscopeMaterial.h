
/***********************************************************************************************/
// Diplomov� pr�ce - Elektronov� raytracer
// DIP2019
// Autor: Jan Such�nek
// Vedouc�: Doc. Ing. Martin �ad�k, Ph.D.
//
///////////////////////////////////////////////////////////////////////////////////////////////
//
// Soubor: microscopeMaterial base class
/***********************************************************************************************/


#if defined(_MSC_VER)
#define NOMINMAX
#pragma once
#endif

#ifndef PBRT_MATERIALS_ELECTRON_BASE_H
#define PBRT_MATERIALS_ELECTRON_BASE_H

// materials/plastic.h*
#include "material.h"
#include "pbrt.h"

namespace pbrt {

// PlasticMaterial Declarations
class MicroscopeMaterial : public Material {
  public:
      //
        // From Experimental data, needed for calculation of etha (Novak, strana 19)
      //
    float BFactor = 0.89f; 
    
    virtual float GetSEYield(float phi) const = 0;

    float secans(float phi) const { return 1.0 / cos(phi); }

};
    

}  // namespace pbrt

#endif  // PBRT_MATERIALS_ELECTRON_BASE_H
