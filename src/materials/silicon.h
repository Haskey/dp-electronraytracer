/***********************************************************************************************/
// Fakulta informatiky, Vysok� u�en� technick� v Brn�
// Diplomov� pr�ce - Elektronov� raytracer
// DIP2019
// Autor: Jan Such�nek
// Vedouc�: Doc. Ing. Martin �ad�k, Ph.D.
//
/////////////////////////////////////////////////////////////////////////////////////////////// 
//
// Soubor: Silicon.h 
// Definice nov�ho materi�lu - k�em�k
/***********************************************************************************************/

#if defined(_MSC_VER)
#define NOMINMAX
#pragma once
#endif

#ifndef PBRT_MATERIALS_SILICON_H
#define PBRT_MATERIALS_SILICON_H

// materials/plastic.h*
#include "microscopeMaterial.h"
#include "pbrt.h"

namespace pbrt {

// PlasticMaterial Declarations
class SiliconMaterial : public MicroscopeMaterial {
  public:
    // PlasticMaterial Public Methods
    SiliconMaterial(const std::shared_ptr<Texture<Spectrum>> &Kd,
                    const std::shared_ptr<Texture<Spectrum>> &Ks,
                    const std::shared_ptr<Texture<Float>> &roughness,
                    const std::shared_ptr<Texture<Float>> &bumpMap,
                    bool remapRoughness)
        : Kd(Kd),
          Ks(Ks),
          roughness(roughness),
          bumpMap(bumpMap),
          remapRoughness(remapRoughness) {}
    void ComputeScatteringFunctions(SurfaceInteraction *si, MemoryArena &arena,
                                    TransportMode mode,
                                    bool allowMultipleLobes) const;

    float etha0 = 0.15f; // for BSE
    float delta0 = 1.0f; // for SE

  private:
    // PlasticMaterial Private Data
    std::shared_ptr<Texture<Spectrum>> Kd, Ks;
    std::shared_ptr<Texture<Float>> roughness, bumpMap;
    const bool remapRoughness;


// Inherited via MicroscopeMaterial
    virtual float GetSEYield(float phi) const override;

    float GetEthaForAngle(float angle) const;
    float GetDeltaForAngle(float angle) const;
};

SiliconMaterial *CreateSiliconMaterial(const TextureParams &mp);

}  // namespace pbrt

#endif  // PBRT_MATERIALS_SILICON_H
